
import UIKit

class cellDetailsViewController: UIViewController {
    private let selectedCellImageFileName:String
    private let selectedCellTitle: String
    private let selectedCellDescription: String
        
    private lazy var cellImageView: UIImageView = {
        let _cellImageView = UIImageView()
        _cellImageView.clipsToBounds = true
        _cellImageView.backgroundColor = .green
        _cellImageView.tintColor = .darkGray
        _cellImageView.layer.cornerRadius = 10
        return _cellImageView
    }()
    
    private lazy var cellTitle: UILabel = {
        let _cellTitle = UILabel()
        _cellTitle.font = UIFont.boldSystemFont(ofSize: 17)
        _cellTitle.text = "_Title"
        return _cellTitle
    }()
    
    private lazy var cellDescription: UILabel = {
        let _cellDescription = UILabel()
        _cellDescription.clipsToBounds = true
        _cellDescription.textColor = .systemGray
        _cellDescription.text = "_Description"
        return _cellDescription
    }()
    
    private lazy var stackView: UIStackView = {
        let _stackView = UIStackView()
        _stackView.axis = .vertical
        _stackView.alignment = .center
        _stackView.spacing = 15
        return _stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.tintColor = .black
        stackView.addArrangedSubview(cellTitle)
        stackView.addArrangedSubview(cellDescription)
        
        view.addSubview(cellImageView)
        view.addSubview(stackView)
        

        cellTitle.text = selectedCellTitle
        cellDescription.text = selectedCellDescription
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        cellTitle.translatesAutoresizingMaskIntoConstraints = false
        cellDescription.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            cellImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            cellImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            cellImageView.heightAnchor.constraint(equalToConstant: 120),
            cellImageView.widthAnchor.constraint(equalToConstant: 120),
            
            stackView.topAnchor.constraint(equalTo: cellImageView.bottomAnchor, constant: 20),
            stackView.heightAnchor.constraint(equalToConstant: 50),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
    
    init(selectedCellTitle: String, selectedCellDescription: String, imageFileName: String){
        self.selectedCellTitle = selectedCellTitle
        self.selectedCellDescription = selectedCellDescription
        self.selectedCellImageFileName = imageFileName
        super.init(nibName: nil, bundle: nil)
        self.cellImageView = UIImageView(image: UIImage(named: imageFileName))
        cellImageView.tintColor = .darkGray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
