

import UIKit
class ViewController: UIViewController {
    
    lazy var tableView = UITableView(frame: .zero, style: .plain)
    lazy var editButton = UIButton()
    lazy var cellsData: [SingleCellData] = []
    
    struct Cells{
        static let theCell = "TableViewCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        configureTableView()
        cellsData = fetchData()
        tableView.isEditing = false
    }
    
    func configureTableView(){
        view.addSubview(tableView)
        view.addSubview(editButton)
        
        setButton()
        setTableViewDelegates()
        tableView.rowHeight = 80
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)
        tableView.pin(to: view)
    }
    
    func setTableViewDelegates(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setButton(){
        editButton.translatesAutoresizingMaskIntoConstraints = false
        editButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        editButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        editButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        editButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        editButton.setTitle("edit mode", for: .normal)
        editButton.setTitleColor(.white, for: .normal)
        editButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        editButton.backgroundColor = .gray
        editButton.layer.cornerRadius = 20
        editButton.addTarget(self, action: #selector (switchEditMode), for: .touchUpInside)
    }
    
    @objc func switchEditMode(){
        if  !tableView.isEditing{
            tableView.isEditing = true
            editButton.setTitleColor(.red, for: .normal)
        }
        else{
            tableView.isEditing = false
            editButton.setTitleColor(.white, for: .normal)
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier) as! TableViewCell
        let singleCell = cellsData[indexPath.row]
        cell.set(singleCellData: singleCell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = cellDetailsViewController(
            selectedCellTitle: cellsData[indexPath.row].title,
            selectedCellDescription: cellsData[indexPath.row].description,
            imageFileName: cellsData[indexPath.row].imageFileName)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        cellsData.swapAt(sourceIndexPath.row, destinationIndexPath.row)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            cellsData.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
 
}
