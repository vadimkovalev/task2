import UIKit

extension ViewController {
    func fetchData() -> [SingleCellData]{
        var _singleCellData: SingleCellData = SingleCellData(image:UIImage(named: iconNames.icons[3]), title: "Title", description: "Descript")
        var _cellsData : [SingleCellData] = []
        for _ in 1...100{
            for i in 1...10 {
                _singleCellData = SingleCellData(imageFileName: iconNames.icons[i-1],
                                                 image:UIImage(named: iconNames.icons[i-1]),
                                                 title: "Title\(i)",
                                                 description: "Description \(i)")
                _cellsData.append(_singleCellData)
            }
        }
        return _cellsData
    }
}
