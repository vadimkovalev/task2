
import UIKit

class TableViewCell: UITableViewCell {
    
    static let identifier = "tableViewCell"
    var imageFileName : String = ""
    
    private lazy var cellImageView: UIImageView = {
        let _cellImageView = UIImageView()
        _cellImageView.clipsToBounds = true
        _cellImageView.tintColor = .darkGray
        _cellImageView.layer.cornerRadius = 10
        return _cellImageView
    }()
    
    private lazy var cellTitle: UILabel = {
        let _cellTitle = UILabel()
        _cellTitle.font = UIFont.boldSystemFont(ofSize: 17)
        _cellTitle.text = "_Title"
        return _cellTitle
    }()
    
    private lazy var cellDescription: UILabel = {
        let _cellDescription = UILabel()
        _cellDescription.clipsToBounds = true
        _cellDescription.textColor = .systemGray
        _cellDescription.text = "_Description"
        return _cellDescription
    }()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(cellImageView)
        contentView.addSubview(cellTitle)
        contentView.addSubview(cellDescription)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cellImageView.frame = CGRect(x: 10,
                                     y: 10,
                                     width: contentView.frame.size.height-20,
                                     height: contentView.frame.size.height-20)
        cellTitle.frame = CGRect(x: 90, y: 10, width: contentView.frame.size.width - 105, height:30)
        cellDescription.frame = CGRect(x: 90, y: 40, width: contentView.frame.size.width - 105, height: 30)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(singleCellData: SingleCellData) {
        cellImageView.image = singleCellData.image
        cellTitle.text = singleCellData.title
        cellDescription.text = singleCellData.description
        imageFileName = singleCellData.imageFileName
    }
    
    override func prepareForReuse() {
        cellImageView.image = nil
        cellTitle.text = nil
        cellDescription.text = nil
    }
    
}
