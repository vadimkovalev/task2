

import UIKit

struct SingleCellData{
     var imageFileName = "lock"
     var image: UIImage? = UIImage(named: "lock") ?? #imageLiteral(resourceName: "baseline_lock_black_48pt_1x.png")
     var title: String
     var description: String
}
